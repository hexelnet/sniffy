# sniffy

A simple function for debugging stuff like promise chains, streams and other pipeline-like thingees. It just creates a function that console logs whatever is passed to it and then returns that value

This package is originally created by [MPJ](https://github.com/mpj).

I only copied it and made it compatible with react-native by removing the methods that require the `util` or `fs` module. I'm so lazy I even copied his README file :) 

If you're not working in a React Native project your better of installing the supersniff module:

[supersniff](https://github.com/mpj/supersniff)


And if you're looking for an AWESOME programming show on youtube you should definitely check out his channel on <https://www.youtube.com/channel/UCO1cgjhGzsSYb1rsB4bFe4Q>


Example:
```javascript
const sniffy = require('sniffy')
//or
import sniffy from 'sniffy'

fetch(`http://myapi.com/users/${username}.json`))
  .then(response => response.json())
  .then(sniffy) // Will console.log out the parsed json, and return the value,
               // effectively passing it on to the next .then
  .then(user => Promise.all(user.friends.map(friend => getFriend(friendId))))
  .then(friends => /* do even more stuff here */)
```

# Why this is useful?
Lets say that you have a promise chain that looks like this ...
```javascript
getData()
  .then(transformData)
  .then(sortData)
```
... and you are debugging an issue that makes you want to inspect what the data looks like after the transformData operation, but BEFORE the sortData operation. No matter if you want to do this by breakpoint or console.log, you need to wrap transformData in a multiline functions:
```javascript
getData()
  .then(x => {
    console.log(x) // alternatively, breakpoint on the line below
    return transformData(x)
  })
  .then(sortData)
```
It's not a huge hassle, but I found myself doing it a LOT, and with sniffy there is a lot less typing:
```javascript
getData()
  .then(transformData)
  .then(sniffy)
  .then(sortData)
```
Yes, this is stupidly simple, but I've found myself writing this function 40000 times now so I want it on npm, OK? OK!?????

# Overrriding prefix
Sniffy will log to console with a [SNIFFY] prefix but if you want to override it like this:

```javascript
const sniffy = require('sniffy')
//or
import sniffy from 'sniffy'


fetch(`http://myapi.com/users/${username}.json`)
  .then(response => response.json())
  .then(sniffy.tag('MYTAG'))
```

# Sniffing to file

This is available is only the [supersniff](https://github.com/mpj/supersniff) package


# Loading file contents conviniently

This is available is only the [supersniff](https://github.com/mpj/supersniff) package


# Memoizing expensive calls to file

This is available is only the [supersniff](https://github.com/mpj/supersniff) package
