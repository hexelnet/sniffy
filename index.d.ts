declare module '@hexelnet/sniffy' {

  type Sniff = <T>(data: T) => T

  /**
   * Logs a value and return it
   *
   * @param data
   * @returns {data}
   */
  function sniff<T>(data: T): T

  /**
   * Tag a sniff with a custom name
   *
   * @param {string} tagName
   * @returns {Sniff}
   */
  function tag(tagName: string): <T>(data: T) => T;

  sniff.tag = tag


  export = sniff
}
